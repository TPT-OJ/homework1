const user = require('./user');

describe('user', function() {
  it('user(1)', function() {
    expect(user(1)).toMatchSnapshot();
  });
  it('user(56)', function() {
    expect(user(56)).toMatchSnapshot();
  });
  it('user(1345)', function() {
    expect(user(1345)).toMatchSnapshot();
  });
  it('throw error if input is string', function() {
    expect(function() {
      user('string');
    }).toThrow(/id needs to be integer/);
  });
});

