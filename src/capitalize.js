const capitalize2 = require('capitalize');

module.exports = function capitalize(str) {
    if (typeof str !== "string") {
	throw new Error('bad input given');
    }
    return capitalize2(str);
};

